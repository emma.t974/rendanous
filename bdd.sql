-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le :  ven. 31 mai 2019 à 07:07
-- Version du serveur :  10.1.38-MariaDB
-- Version de PHP :  7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `rendez_vous`
--

-- --------------------------------------------------------

--
-- Structure de la table `histo_rdv`
--

CREATE TABLE `histo_rdv` (
  `idHisto` int(11) NOT NULL,
  `action` enum('insert','update') DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `idRdv` int(11) NOT NULL DEFAULT '0',
  `idPerso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `histo_rdv`
--

INSERT INTO `histo_rdv` (`idHisto`, `action`, `date`, `idRdv`, `idPerso`) VALUES
(1, 'insert', '2019-05-31 08:59:17', 0, 1),
(2, 'update', '2019-05-31 09:05:01', 14, 2),
(3, 'update', '2019-05-31 09:06:18', 9, 2),
(4, 'update', '2019-05-31 09:07:33', 11, 2);

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `pseudo` varchar(100) NOT NULL,
  `password` text NOT NULL,
  `idMember` int(11) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `login`
--

INSERT INTO `login` (`id`, `pseudo`, `password`, `idMember`, `status`) VALUES
(1, 'tony.emma', '$2y$10$9XE96BsulKvI/YQ5KFRueeAEi7c1qNgmfwYes4P9Pm0uQGdwox4hS', 1, 'Etudiant'),
(2, 'patrick.combe', '$2y$10$9XE96BsulKvI/YQ5KFRueeAEi7c1qNgmfwYes4P9Pm0uQGdwox4hS', 2, 'Professeur'),
(3, 'pierre.goubeaux', '$2y$10$9XE96BsulKvI/YQ5KFRueeAEi7c1qNgmfwYes4P9Pm0uQGdwox4hS', 3, 'Etudiant'),
(4, 'robert.ammany', '$2y$10$9XE96BsulKvI/YQ5KFRueeAEi7c1qNgmfwYes4P9Pm0uQGdwox4hS', 4, 'Professeur');

-- --------------------------------------------------------

--
-- Structure de la table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `adresse` varchar(50) NOT NULL,
  `telephone` int(11) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `member`
--

INSERT INTO `member` (`id`, `name`, `adresse`, `telephone`, `email`) VALUES
(1, 'EMMA Tony', '7 rue du domaine', 692121014, 'emma.tony.georges@gmail.com'),
(2, 'Patrick Combe', 'ché pas', 692121212, 'patrick.combe@ac-reunion.fr'),
(3, 'Pierre Goubeaux', 'oui', 692121212, 'oui@oui.com'),
(4, 'Robert Ammany', '7 rue du domaine', 692121212, 'test@test.com');

-- --------------------------------------------------------

--
-- Structure de la table `rdv`
--

CREATE TABLE `rdv` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `heure` time NOT NULL,
  `objet` varchar(50) NOT NULL,
  `message` text NOT NULL,
  `reponse` text NOT NULL,
  `joint` varchar(150) NOT NULL,
  `idEtudiant` int(11) NOT NULL,
  `idProfs` int(11) NOT NULL,
  `situation` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `rdv`
--

INSERT INTO `rdv` (`id`, `date`, `heure`, `objet`, `message`, `reponse`, `joint`, `idEtudiant`, `idProfs`, `situation`) VALUES
(1, '2019-04-09', '00:00:00', 'Oui', 'truc', '', '', 1, 2, 'Accepter'),
(2, '2019-04-10', '00:00:00', 'test', 'test ', '', '', 1, 2, 'Accepter'),
(3, '2019-04-18', '00:00:00', 'autre test', 'test ', '', '', 1, 2, 'Refuser'),
(4, '2019-04-24', '00:00:00', 'test', 'test ', '', '', 3, 4, 'Accepter'),
(5, '2019-04-04', '00:00:00', 'truc truc truc', 'truc truc ', '', '', 1, 2, 'Accepter'),
(6, '2019-04-06', '00:00:00', 'lalalalala', 'test ', 'ergeezrazre', '', 1, 4, 'Accepter'),
(7, '0000-00-00', '00:00:00', 'test', 'test ', '', '', 1, 4, 'Refuser'),
(8, '2019-04-10', '00:00:00', 'a', 'a ', 'efarar', '', 1, 2, 'Refuser'),
(9, '2019-04-17', '12:31:00', 'test', 'test ', 'test', '', 1, 2, 'Refuser'),
(10, '2019-04-30', '12:30:00', 'test', 'test ', 'test', '', 1, 2, 'Accepter'),
(11, '2019-05-06', '12:50:00', 'test', ' test', 'a', '', 1, 2, 'Accepter'),
(12, '2019-04-29', '12:21:00', 'test', ' test', '', '', 1, 2, 'En attente'),
(13, '2019-05-06', '12:03:00', 'test', ' test', '', '', 1, 2, 'En attente'),
(14, '2019-05-31', '12:03:00', 'lalalala', 't ', 'test', '', 1, 2, 'Refuser');

--
-- Déclencheurs `rdv`
--
DELIMITER $$
CREATE TRIGGER `trig_avant_insert_rdv` BEFORE INSERT ON `rdv` FOR EACH ROW BEGIN
    INSERT INTO histo_rdv
      (action, date, idRdv, idPerso)
    VALUES
      ('insert', NOW(), new.id, new.idEtudiant);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trig_avant_update_rdv` BEFORE UPDATE ON `rdv` FOR EACH ROW BEGIN
    INSERT INTO histo_rdv
      (action, date, idRdv, idPerso)
    VALUES
      ("update", NOW(), old.id, old.idProfs);
  END
$$
DELIMITER ;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `histo_rdv`
--
ALTER TABLE `histo_rdv`
  ADD PRIMARY KEY (`idHisto`);

--
-- Index pour la table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idMember` (`idMember`);

--
-- Index pour la table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idEtudiant` (`idEtudiant`),
  ADD KEY `idProfs` (`idProfs`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `histo_rdv`
--
ALTER TABLE `histo_rdv`
  MODIFY `idHisto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `rdv`
--
ALTER TABLE `rdv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_ibfk_1` FOREIGN KEY (`idMember`) REFERENCES `member` (`id`);

--
-- Contraintes pour la table `rdv`
--
ALTER TABLE `rdv`
  ADD CONSTRAINT `rdv_ibfk_1` FOREIGN KEY (`idEtudiant`) REFERENCES `member` (`id`),
  ADD CONSTRAINT `rdv_ibfk_2` FOREIGN KEY (`idProfs`) REFERENCES `member` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
