<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="index.php">Rendanous</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
  
  <ul class="nav justify-content-center">
   <li> <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" size="70" aria-label="Search">
      <button class="btn btn-info my-2 my-sm-0" type="submit"><i class="fas fa-search"></i> Search</button>
    </form></li>
  </ul> </div>
    <ul class="nav justify-content-end">
    <li><a href="?ctrl=member&mth=deconnexion" class="btn btn-danger"><i class="fas fa-sign-out-alt"></i> Deconnexion <?php echo $_SESSION['pseudo']; ?> </a></li>
  </ul>

</nav>