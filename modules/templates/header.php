<?php
session_start();
$cookiePseudo = (@$_COOKIE['pseudo'] ? $_COOKIE['pseudo'] : '');
$cookieId = (@$_COOKIE['id'] ? $_COOKIE['id'] : '0');
$cookieStatus = (@$_COOKIE['status'] ? $_COOKIE['status'] : '');

if($cookiePseudo != '' && $cookieId != '0' && $cookieStatus != ''){
	$_SESSION['pseudo'] = $cookiePseudo;
	$_SESSION['id'] = $cookieId;
	$_SESSION['status'] = $cookieStatus;
}

// echo $cookiePseudo;
// echo $cookieId;
// echo $cookieStatus;
?>
<!DOCTYPE HTML>
<html lang="fr">

	<head>
		<title>Rendanous - Site de rendez-vous entre élève et professeur lycée pierre poivre</title>

		<!-- PLUGGING CSS -->
		<link href="template/plugging/bootstrap/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
		<link href="template/plugging/toastr/build/toastr.min.css" rel="stylesheet" />	
		<link href="template/plugging/fontawesome/css/all.css" rel="stylesheet" />	
		<!-- PLUGGING JS -->
		<script src="template/plugging/jquery/jquery-3.3.1.min.js" /></script>
		<script src="template/plugging/toastr/build/toastr.min.js" /></script>
		<script src="template/plugging/bootstrap/js/bootstrap.min.js" /></script>
		<script>toastr.options.positionClass = 'toast-bottom-right';</script>
		<link rel="stylesheet" type="text/css" href="modules/templates/style.css">

		<link rel="icon" type="image/png" href="modules/templates/icon.png" />
	</head>

	<body>