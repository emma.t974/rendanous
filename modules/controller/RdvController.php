<?php
require_once("modules/model/Rdv.php");
require_once("modules/model/Member.php");
class RdvController
{

	private $rdv;

	public function __construct(){
		$this->rdv = new Rdv();
		$this->member = new Member();
	}
	public function consultation()
	{
		session_start();
		$result = $this->rdv->get($_SESSION['id'],$_SESSION['status'],"En attente");
		session_write_close();

		$profs = $this->member->getProfs();
		include("modules/view/rdz/consultation.php");
	}

	public function consultation_accepter()
	{
		session_start();
		$result = $this->rdv->get($_SESSION['id'],$_SESSION['status'],"Accepter");
		session_write_close();
		$profs = $this->member->getProfs();
		include("modules/view/rdz/consultation_accepter.php");
	}

		public function consultation_refuser()
	{
		session_start();
		$result = $this->rdv->get($_SESSION['id'],$_SESSION['status'],"Refuser");
		session_write_close();
		$profs = $this->member->getProfs();
		include("modules/view/rdz/consultation_refuser.php");
	}

	public function contact(){
		session_start();
		$this->rdv->contact($_SESSION['id'],$_POST);
		session_write_close();

		header("Location: index.php?&ctrl=rdv&mth=consultation"); 
	}

	public function read(){
		$result = $this->rdv->getRdv($_GET['id']);

		include("modules/view/rdz/read.php");
	}

	// public function accept(){
	// 	$this->rdv->accept($_GET['id']);

	// 	header("Location: index.php"); 
	// }

	// public function refus(){
	// 	$this->rdv->refus($_GET['id']);

	// 	header("Location: index.php"); 
	// }

	public function response(){
		 $this->rdv->response($_POST);

		 include("modules/view/rdz/response.php");
	}
}
