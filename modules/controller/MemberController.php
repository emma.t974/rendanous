<?php
require_once("modules/model/Member.php");

class MemberController{
	private $member;

	public function __construct(){
		$this->member = new Member();
	}
	public function inscription(){
		include("modules/view/member/registre.php");
	}

	public function login(){
		$result = $this->member->connexion($_POST);

		if($result){
			if(password_verify($_POST['pwd'], $result['password'])){
				session_start();
				$_SESSION['pseudo'] = $result['pseudo'];
				$_SESSION['id'] = $result['idMember'];
				$_SESSION['status'] = $result['status'];

				//Parti cookie
				if(@$_POST['auto']){
					setcookie('pseudo',$result['pseudo'], time() + 365*24*3600, null, null, false, true);
					setcookie('id',$result['idMember'], time() + 365*24*3600, null, null, false, true);
					setcookie('status',$result['status'], time() + 365*24*3600, null, null, false, true);
				}


				header("Location: index.php?&ctrl=rdv&mth=consultation");
			}
			else {
				$msg = "Erreur dans votre mot de passe";
				include('modules/view/member/login.php');
			}
		}
		else {
			$msg = "Erreur dans votre identifiant/mot de passe";
			include('modules/view/member/login.php');
		}
	}

	public function deconnexion(){

		//Supprime les cookies
		setcookie('pseudo','', time() + 365*24*3600, null, null, false, true);
		setcookie('id','0', time() + 365*24*3600, null, null, false, true);
		setcookie('status','', time() + 365*24*3600, null, null, false, true);

		include("modules/view/member/deconnexion.php");
	}
}