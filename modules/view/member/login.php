<?php include("modules/templates/header.php"); ?>
<?php if(@$_SESSION['pseudo']) 
{
  header("Location: index.php?&ctrl=rdv&mth=consultation");
}
else{
?>
<link href="modules/templates/login.css" rel="stylesheet">

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

  <body class="text-center">
  	<aside>
    <form class="form-signin" method="post" action="?ctrl=Member&mth=login">
  <img src="modules/templates/logo_lycee.jpg" >
  <h1 class="h3 mt-2 mb-3 font-weight-normal"><i class="fas fa-user"></i> CONNEXION</h1>
  <label for="inputEmail" class="sr-only">Nom d'utilisateur</label>
  <input type="text" id="pseudo" class="form-control" placeholder="Nom d'utilisateur" name="pseudo" required autofocus>
  <label for="inputPassword" class="sr-only">Mot de passe</label>
  <input type="password" id="pwd" class="form-control" placeholder="Mot de passe" name="pwd" required>

   <label for="recaptcha"> <div class="g-recaptcha" data-sitekey="6LcL150UAAAAANOjjzRs_y34zsBprFmWTcsFXCmS" data-callback="recaptchaCallback" data-expired-callback="recaptchaExpired"></div></label>

  <div class="checkbox mb-3">
    <label>
      <input type="checkbox" id="auto" name="auto"> Me souvenir
    </label>
  </div>
  <input type="submit" class="btn btn-lg btn-primary btn-block" name="submit" id="submit" value="Valider" disabled>
  </form>
  <?php echo @$msg; ?>
  <p class="mt-2">
   <a href="?ctrl=Member&mth=inscription">Pas encore inscris ?</a><br>
<span class="text-muted">En vous connectant, vous acceptez le système de cookie en cas si vous avez cochez "Me souvenir" pour la connexion automatique</span></p>
  <p class="mt-5 mb-3 text-muted">&copy; 2017-2019 Site pédagogie</p>

</aside>
</body>

<script>
    function recaptchaCallback(){
      $('#submit').prop('disabled', false);
     }

  function recaptchaExpired(){
    $('#submit').prop('disabled', true);
  }
  </script>

</html>


<?php } ?>
