<?php include("modules/templates/header.php"); ?>
<?php if(@$_SESSION['pseudo']) {
include("modules/templates/nav-top.php");
?>
<style>
  body{
    background: white;
  }

table {
  border-collapse: collapse;
  border-spacing: 0;
  text-align: center;
}

td,
th {
  padding: 5px;
  border-bottom: 1px solid #aaa;
}
  section{
  display : flex; 
  padding-top : 0.3%; 
}
#menu{
margin-left: 1.5%;
margin-right: 30px;
position: absolute;
box-shadow : 0px 2px 5px black;
border-radius : 5px;
background-color: white;
padding : 10px;
}
#contenu{
   margin-left: 25%;
position: relative;
flex: 3 ;

}
</style><br>
<!-- Bouton add -->
<div id="menu">
<?php 
if(@$_SESSION['status'] == "Etudiant")
  include("add.php");
?>
<ul>
  <li><a href="?ctrl=rdv&mth=consultation">En attente</a></li>
  <li><a href="?ctrl=rdv&mth=consultation_accepter">Accepter</a></li>
  <li><a href="?ctrl=rdv&mth=consultation_refuser">Refuser</a></li>
</ul>
</div>
<!-- Pour savoir lequel est actif -->
<section>
<div id="contenu">

<?php
      $date = date_create($result['date']);
      $heure = date_create($result['heure']); 
?>
<!-- Affiche les résultat -->

Date du rendez-vous : <?php echo date_format($date, 'd/m/Y'); ?> à <?php echo date_format($heure, 'H:i'); ?> <br>
État du rendez-vous : <b><?php echo $result['situation']; ?> </b><br>
Message de l'élève : <br> <?php echo $result['message']; ?>
<hr>
Réponse du professeurs : <br>
<?php if($result['reponse'] == "" && $_SESSION['status'] == "Etudiant") echo "Votre professeurs n'a pas encore répondu"; else echo $result['reponse']; ?>
<?php if($_SESSION['status'] == "Professeur" && $result['situation'] == "En attente" ){
  ?>
  <form method="post" action="?ctrl=rdv&mth=response">
    <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
  Réponse : <textarea name="message" required></textarea><br>
  <input type="radio" name="choix" value="Accepter"> Accepter <input type="radio" name="choix" value="Refuser"> Refuser<br>

  <input type="submit" value="Répondre" class="btn btn-primary">
</form>
<?php } else if($_SESSION['status'] == "Professeur" && $result['situation'] != "En attente") { echo "<p><hr>Vous ne pouvez pu répondre</p>"; } ?>
</div>
  </section>
  <?php }
  else
    echo "<p>Vous n'avez pas de rendez-vous</p>"; ?>
