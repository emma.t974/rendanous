<?php include("modules/templates/header.php"); ?>
<?php if(@$_SESSION['pseudo']) {
include("modules/templates/nav-top.php");
?>
<style>
  body{
    background: white;
  }

table {
  border-collapse: collapse;
  border-spacing: 0;
  text-align: center;
}

td,
th {
  padding: 5px;
  border-bottom: 1px solid #aaa;
}
  section{
  display : flex; 
  padding-top : 0.3%; 
}
#menu{
margin-left: 1.5%;
margin-right: 30px;
position: absolute;
box-shadow : 0px 2px 5px black;
border-radius : 5px;
background-color: white;
padding : 10px;
}
#contenu{
   margin-left: 25%;
position: relative;
flex: 3 ;

}
</style><br>
<!-- Bouton add -->
<div id="menu">
<?php 
if(@$_SESSION['status'] == "Etudiant")
  include("add.php");
?>
<ul>
  <li><a href="?ctrl=rdv&mth=consultation">En attente</a></li>
  <li><a href="?ctrl=rdv&mth=consultation_accepter">Accepter</a></li>
  <li><a href="?ctrl=rdv&mth=consultation_refuser">Refuser</a></li>
</ul>
</div>
<!-- Pour savoir lequel est actif -->
<section>
<div id="contenu">

<!-- Affiche les résultat -->

<?php 
//var_dump($result);
if(@$result){
  ?>
  <table>
    <thead>
      <tr>
        <th>Nom </th>
        <th>Objet</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
  <?php
     foreach ($result as $key => $value){
      $date = date_create($value['date']);
      $heure = date_create($value['heure']);
    ?>
    <tr>
    <td><?php echo $value['name']; ?></td>
    <td><a href="?ctrl=rdv&mth=read&id=<?php echo $value['idrdv']; ?>"><?php echo $value['objet']; ?></a></td>
    <td><?php echo date_format($date, 'd/m/Y'); ?> à <?php echo date_format($heure, 'H:i'); ?></td>
 </tr>
  <?php }
  ?>
</tbody>

   </table>
</div>
  </section>
  <?php }
  else
    echo "<p>Vous n'avez pas de rendez-vous</p>"; ?>


<?php }
else{
header("Location: index.php"); } ?>
