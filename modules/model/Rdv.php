<?php
require_once("ConnexionDB.php");
class Rdv extends ConnexionDB{

	// Code pour récupéré tous les rendez-vous par rapport si c'est un professeurs/élève et sa situation
	public function get($id,$status,$cdt){
		if($status == "Etudiant"){
			$req = $this->cnx->prepare("SELECT rdv.id as 'idrdv' , name ,date, heure, objet FROM rdv,member where rdv.idProfs = member.id and idEtudiant=? and situation=?");
			$req->execute(array($id,$cdt));
		}
		else if($status == "Professeur"){
			$req = $this->cnx->prepare("SELECT rdv.id as 'idrdv' , name ,date, heure, objet  FROM rdv,member where rdv.idEtudiant = member.id and idProfs=? and situation=?");
			$req->execute(array($id,$cdt));
		}

		return $req->fetchAll();
	}

	// Code quand un élève souhaite contacter un professeur
	public function contact($id,$post){
		$req = $this->cnx->prepare("INSERT INTO rdv (date,heure,objet,message,idEtudiant,idProfs,situation) VALUES (?,?,?,?,?,?,?)");
		$req->execute(array($post['date'],$post['heure'],$post['objet'],$post['message'],$id,$post['profs'],"En attente"));
	}

	// Code qui permet de voir un rendez-vous
	public function getRdv($id){
		$req = $this->cnx->prepare("SELECT * FROM rdv where id=?");
		$req->execute(array($id));

		return $req->fetch();
	}

	// public function accept($id){
	// 	$req = $this->cnx->prepare("UPDATE rdv SET situation='Accepter' where id=?");
	// 	$req->execute(array($id));
	// }

	// public function refus($id){
	// 	$req = $this->cnx->prepare("UPDATE rdv SET situation='Refuser' where id=?");
	// 	$req->execute(array($id));
	// }

	// Code qui permet à un professeurs de répondre
	public function response($post){
		$req = $this->cnx->prepare("UPDATE rdv SET situation=?,reponse=? where id=?");
		$req->execute(array($post['choix'], $post['message'], $post['id']));
	}
}