<?php
require_once("ConnexionDB.php");
class Member extends ConnexionDB{
	
	//Permet de récupéré les information sur une personne
	public function get($id){
		$req = $this->cnx->prepare("SELECT * from login,member where login.idMember = member.id Where id = ?");
		$req->execute(array($id));

		return $req->fetch();
	}

	//Permet de récupéré tous les informations

	public function getAll(){
		$req = $this->cnx->prepare("SELECT * from login, member where login.idMember = member.id");
		$req->execute();

		return $req->fetchAll();
	}

	//Permet de récupérer la liste de tous les professeurs
	public function getProfs(){
		$req = $this->cnx->prepare("SELECT * from login, member where login.idMember = member.id and status='Professeur'");
		$req->execute();

		return $req->fetchAll();
	}

	//Fonction pour permet de s'inscrire
	public function inscription($post){

		//POUR LA TABLE MEMBER
		$req = $this->cnx->prepare("INSERT INTO member(name,adresse,telephone,mail) VALUES (?,?,?,?)");
		$req->execute(array($post['name'],$post['adresse'],$post['telepohone'],$post['mail']));

		unset($req);

		//POUR LA TABLE LOGIN
		$id = $this->cnx->lastInsertId();
		$pwd = password_hash($post['pwd'], PASSWORD_DEFAULT);

		//ON SÉPARE LE NOM ET LE PRÉNOM
		$separename = explode(" ",$post['name']);

		//ON FUSIONNE LE NOM ET PRÉNOM ET ON FORCE LE TYPE EN MINISCULE
		$pseudo = strtolower($separename[0].".".$separename[1]);


		$req = $this->cnx->prepare("INSERT INTO login(pseudo,password,idMember) VALUES (?,?,?)");
		$req->execute(array($pseudo,$pwd,$id));

		return $req->rowCount();
	}

	//connexion
	public function connexion($post){
		//ON FORCE LA SAISIR EN MINISCULE
		$pseudo = strtolower($post['pseudo']); 


		$req = $this->cnx->prepare("SELECT pseudo,password, idMember, status FROM login where pseudo=?");
		$req->execute(array($pseudo));

		return $req->fetch();
		
	}
}